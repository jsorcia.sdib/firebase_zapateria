package com.kary.zapateriafirebase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private static final String PATH_ZAPATO = "zapatos";
    static final int REQUEST_IMAGE_CAPTURE = 1;
    EditText precio , modelo , color;
    RatingBar valoracion;
    Button btnGuardar , btnFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        inicializarComponentes();
    }

    public void inicializarComponentes(){
        precio = findViewById(R.id.txtPrecio);
        modelo = findViewById(R.id.txtModelo);
        color = findViewById(R.id.txtColor);
        valoracion = findViewById(R.id.valoracion);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Zapato zapato = new Zapato(modelo.getText().toString(),color.getText().toString(),Double.parseDouble(precio.getText().toString()),valoracion.getRating(),"DATA/IMAGE/PRUEBA.PNG");
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference(PATH_ZAPATO);
                myRef.push().setValue(zapato);
                Snackbar.make(view, "Registro Realizado", Snackbar.LENGTH_LONG)
                        .show();
                precio.setText("");
                modelo.setText("");
                color.setText("");
                valoracion.setNumStars(0);
            }
        });
        btnFoto = findViewById(R.id.btnFoto);
        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });
    }
}