package com.kary.zapateriafirebase;

public class Zapato {
    private String modelo;
    private String color;
    private double precio;
    private float valoracion;
    private  String urlFoto;
    private  int estatus;

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public float getValoracion() {
        return valoracion;
    }

    public void setValoracion(float valoracion) {
        this.valoracion = valoracion;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public Zapato(String modelo, String color, double precio, float valoracion, String urlFoto) {
        this.modelo = modelo;
        this.color = color;
        this.precio = precio;
        this.valoracion = valoracion;
        this.urlFoto = urlFoto;
        this.estatus = 0;
    }
}
